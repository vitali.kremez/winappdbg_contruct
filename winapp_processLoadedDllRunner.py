import winappdbg
import argparse
from winappdbg import win32
class DebugEvents(winappdbg.EventHandler):
    """
    Event handler class.
    event: https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/event.py
    """

    def load_dll(self, event):
        """
        Called when a new module is loaded.
        """
        # https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/module.py#L71

        """
        Module object methods:
        get_base, get_filename, get_name, get_size, get_entry_point,
        get_process, set_process, get_pid,
        get_handle, set_handle, open_handle, close_handle

        get_size and get_entry_point do not work because WinAppDbg internally
        uses GetModuleInformation.
        https://msdn.microsoft.com/en-us/library/ms683201(v=VS.85).aspx
        And it cannot get info on files with "that were loaded with the
        LOAD_LIBRARY_AS_DATAFILE flag.""

        This is related:
        https://blogs.msdn.microsoft.com/oldnewthing/20150716-00/?p=45131
        """
        module = event.get_module()

        logstring = "\nLoaded DLL:\nName: %s\nFilename: %s\nBase Addr: %s\n" % \
            (module.get_name(), module.get_filename(), hex(module.get_base()))

        logger.log_text(logstring)

        # _ = module.get_size()

        print module.get_handle()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="WinAppDBG Trainer.")
    parser.add_argument("-r", "--run",  nargs="+", help="full path to executable")
    parser.add_argument("-i", "--sysinfo", action="store_true",
                        help="print system information")
    parser.add_argument("-p", "--getprocesses", action="store_true",
                        help="print processes information")
    parser.add_argument("-pi", "--pid", type=int, dest="pid", help="attach to process id")
    parser.add_argument("-o", "--output", dest="output", help="log filename")

    args = parser.parse_args()
    global logger
    if args.output:
        # verbose=False disables printing to stdout
        logger = winappdbg.Logger(args.output, verbose=False)
    else:
        logger = winappdbg.Logger()
    myeventhandler = DebugEvents()

if (args.run):
    # Concat all arguments into a string
    myargs = " ".join(args.run)

    if win32.PathFileExists(args.run[0]) is True:
        # File exists
        # Create a Debug object
        debug = winappdbg.Debug(myeventhandler)

        try:
            # Debug the app
            # First item is program and the rest are arguments
            # execl: https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/debug.py#L358
            my_process = debug.execl(myargs)

            print("Attached to {0} - {1}".format(my_process.get_pid(),
                                                 my_process.get_filename()))

            # Keep debugging until the debugger stops
            debug.loop()

        finally:
            # Stop the debugger
            debug.stop()
            print("[*] Debugger stopped.")

    else:
        print("{0} not found.".format(args.run[0]))
        exit()

if(args.sysinfo):
        # Create a System object
        # https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/system.py#L66
        system = winappdbg.System()

        # Use the built-in WinAppDbg table
        # https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/textio.py#L1094
        table = winappdbg.Table("\t")

        # New line
        table.addRow("", "")

        # Header
        title = ("System Information", "")
        table.addRow(*title)

        # Add system information
        table.addRow("------------------")
        table.addRow("Bits", system.bits)
        table.addRow("OS", system.os)
        table.addRow("Architecture", system.arch)
        table.addRow("32-bit Emulation", system.wow64)
        table.addRow("Admin", system.is_admin())
        table.addRow("WinAppDbg", winappdbg.version)
        table.addRow("Process Count", system.get_process_count())

        print table.getOutput()
        exit()