import Tkinter as tk
import Tkinter, Tkconstants, tkFileDialog
import winappdbg
from winappdbg import win32
from Tkinter import *
import tkMessageBox
import ttk


class system_information():
    def __init__(self):
        self.info_list = ''

    def retrieve_sysinfo(self):
        # Create a System object
        # https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/system.py#L66
        system = winappdbg.System()

        # Use the built-in WinAppDbg table
        # https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/textio.py#L1094
        table = winappdbg.Table("\t")

        # New line
        table.addRow("", "")

        # Header
        title = ("System Information", "")
        table.addRow(*title)

        # Add system information
        table.addRow("------------------")
        table.addRow("Bits", system.bits)
        table.addRow("OS", system.os)
        table.addRow("Architecture", system.arch)
        table.addRow("32-bit Emulation", system.wow64)
        table.addRow("Admin", system.is_admin())
        table.addRow("WinAppDbg", winappdbg.version)
        table.addRow("Process Count", system.get_process_count())

        self.info_list = table.getOutput()
        return self.info_list

class process():
    def __init__(self):
        self.process_list = ''

    def process_retrieve(self):
        system = winappdbg.System()

        # We can reuse example 02 from the docs
        # https://winappdbg.readthedocs.io/en/latest/Instrumentation.html#example-2-enumerating-running-processes
        table = winappdbg.Table("\t")
        table.addRow("", "")

        header = ("Process ID ", "Process Name")
        table.addRow(*header)

        table.addRow("----", "----------")

        processes = {}
        # Add all processes to a dictionary then sort them by pid
        for process in system:
            processes[process.get_pid()] = process.get_filename()

        # Iterate through processes sorted by pid
        for key in sorted(processes.iterkeys()):
            table.addRow(key, processes[key])
        self.process_list = table.getOutput()
        return self.process_list

def print_process():
        d = process()
        d.process_retrieve()
        tkMessageBox.showinfo("PROCESS LIST", d.process_list)

def donothing():
    x = 0

def helloCallBack():
   tkMessageBox.showinfo( "Credit", "[*] Made by VK_")

if __name__ == "__main__":

    root = Tk()
    root.title("[*] Welcome to MalInspector")
    root.geometry("800x600")
    btn = Button(root, text="Refresh Process List", command=print_process, bd =5)
    btn.grid(column=0, row=0)
    btn.pack()
    ttk.Separator(root).place(x=0, y=1, relwidth=1)
    # open ask
    # Credit button implemenation
    B = Button(root, text="Credit", command=helloCallBack)
    B.grid(column=0, row=0)
    B.pack()
    # Get user input implementaiton
    var = StringVar()
    textbox = Entry(root, textvariable=var, bd =5)
    textbox.focus_set()
    textbox.pack(pady=10, padx=10)
    ttk.Separator(root).place(x=0, y=26, relwidth=1)

    # menu implementation
    menubar = Menu(root)
    filemenu = Menu(menubar, tearoff=0)
    filemenu.add_command(label="New", command=donothing)
    filemenu.add_command(label="Open", command=donothing)
    filemenu.add_command(label="Save", command=donothing)
    filemenu.add_separator()
    filemenu.add_command(label="Exit", command=root.quit)
    menubar.add_cascade(label="File", menu=filemenu)
    helpmenu = Menu(menubar, tearoff=0)
    helpmenu.add_command(label="Help Index", command=donothing)
    helpmenu.add_command(label="About...", command=donothing)
    menubar.add_cascade(label="Help", menu=helpmenu)
    root.config(menu=menubar)
    # retreive system information
    S = Text(root, height=30, width=34, bd =5)
    S.pack(padx=0, pady=10, side=LEFT)
    s = system_information()
    s.retrieve_sysinfo()
    S.insert(END, s.info_list)
    # process retrieve implementation
    P = Text(root, height=100, width=80, bd =5)
    P.pack(padx=0, pady=10, side=LEFT)
    d = process()
    d.process_retrieve()
    P.insert(END, d.process_list)
    root.mainloop()
