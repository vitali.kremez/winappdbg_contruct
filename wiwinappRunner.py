import winappdbg
import argparse
from winappdbg import win32

# /////////////////////////////
# WinAppdbg Trainer for run / sysinfo / process information
# /////////////////////////////
# Sample usage: python winappRunner.py -r c:\windows\system32\notepad.exe
# Sample usage: python winappRunner.py -p
# Sample usage: python winappRunner.py -i


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="WinAppDBG Trainer.")
    parser.add_argument("-r", "--run",  nargs="+", help="full path to executable")
    parser.add_argument("-i", "--sysinfo", action="store_true",
                        help="print system information")
    parser.add_argument("-p", "--getprocesses", action="store_true",
                        help="print processes information")
    parser.add_argument("-pi", "--pid", type=int, dest="pid", help="attach to process id")
    parser.add_argument("-o", "--output", dest="output", help="log filename")

    args = parser.parse_args()

    if (args.run):
        # Concat all arguments into a string
        myargs = " ".join(args.run)

        if win32.PathFileExists(args.run[0]) is True:
            # File exists
            # Create a Debug object
            debug = winappdbg.Debug()

            try:
                # Debug the app
                # First item is program and the rest are arguments
                # execl: https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/debug.py#L358
                my_process = debug.execl(myargs)

                print("Attached to {0} - {1}".format(my_process.get_pid(),
                                               my_process.get_filename()))

                # Keep debugging until the debugger stops
                debug.loop()

            finally:
                # Stop the debugger
                debug.stop()
                print("[*]Debugger stopped.")

        else:
            print("{0} not found.".format(args.run[0]))
        exit()

    global logger
    if args.output:
        # verbose=False disables printing to stdout
        logger = winappdbg.Logger(args.output, verbose=False)
    else:
        logger = winappdbg.Logger()

    if(args.sysinfo):
        # Create a System object
        # https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/system.py#L66
        system = winappdbg.System()

        # Use the built-in WinAppDbg table
        # https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/textio.py#L1094
        table = winappdbg.Table("\t")

        # New line
        table.addRow("", "")

        # Header
        title = ("System Information", "")
        table.addRow(*title)

        # Add system information
        table.addRow("------------------")
        table.addRow("Bits", system.bits)
        table.addRow("OS", system.os)
        table.addRow("Architecture", system.arch)
        table.addRow("32-bit Emulation", system.wow64)
        table.addRow("Admin", system.is_admin())
        table.addRow("WinAppDbg", winappdbg.version)
        table.addRow("Process Count", system.get_process_count())

        print table.getOutput()

        exit()

    # If no arguments, print running processes
    if (args.getprocesses):

        system = winappdbg.System()

        # We can reuse example 02 from the docs
        # https://winappdbg.readthedocs.io/en/latest/Instrumentation.html#example-2-enumerating-running-processes
        table = winappdbg.Table("\t")
        table.addRow("", "")

        header = ("pid", "process")
        table.addRow(*header)

        table.addRow("----", "----------")

        processes = {}

        # Add all processes to a dictionary then sort them by pid
        for process in system:
            processes[process.get_pid()] = process.get_filename()

        # Iterate through processes sorted by pid
        for key in sorted(processes.iterkeys()):
            table.addRow(key, processes[key])

        print table.getOutput()
        logger.log_text("Started %d - %s" % (process.get_pid(), process.get_filename()))
        logger.log_text(table.getOutput())
        exit()

    if (args.pid):
        system = winappdbg.System()
        pids = system.get_process_ids()
        if args.pid in pids:
            # pid exists

            # Create a Debug object
            debug = winappdbg.Debug()
            try:
                # Attach to pid
                # attach: https://github.com/MarioVilas/winappdbg/blob/master/winappdbg/debug.py#L219
                my_process = debug.attach(args.pid)

                print "Attached to %d - %s" % (my_process.get_pid(),
                                               my_process.get_filename())

                # Keep debugging until the debugger stops
                debug.loop()

            finally:
                # Stop the debugger
                debug.stop()
                print "Debugger stopped."

        else:
            print "pid %d not found." % (args.pid)

        exit()